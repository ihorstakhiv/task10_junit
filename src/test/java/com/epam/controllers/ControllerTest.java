package com.epam.controllers;

import org.junit.Rule;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.rules.TestRule;
import org.junit.rules.Timeout;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ControllerTest {

    private final static boolean bug = false;
    private static int count = 0;

    @InjectMocks
    Controller controller;

    @Rule
    public TestRule timeout = new Timeout(1000);

    @RepeatedTest(10)
    void testMineAt() {
        count++;
        assertEquals(0, new Controller().mineAt(5, 20));
    }

    @Test
    void test2MineAt() {
        count++;
        if (bug) {
            fail("you have a problem");
        } else assertNotEquals(new Controller().mineAt(1, 3), 1);
    }

    @Test
    public void test3MineAt() {
        count++;
        assertThrows(Exception.class, () -> new Controller().mineAt(4, 1));
    }

    @Test
    public void test4MineAt() {
        Controller mock = Mockito.mock(Controller.class);
        when(mock.mineAt(1, 2)).thenReturn(0);
        assertEquals(mock.mineAt(1, 2), 0);

        count++;
    }
}
