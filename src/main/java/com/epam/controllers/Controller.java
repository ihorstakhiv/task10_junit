package com.epam.controllers;

import com.epam.models.Board;
import java.util.Random;

public class Controller {

    private Board board;
    private Random random;

    public Controller() {
        board = new Board();
        random = new Random();
    }

    public char[][] fillUpBoard(int mHeight, int mWidth) {
        board.setmHeight(mHeight);
        board.setmWidth(mWidth);
        return board.setmMinefield(new char[board.getmHeight()][board.getmWidth()]);
    }

    public void placeMines(int minesCount) {
        board.setmMines(minesCount);
        int minesPlaced = 0;
        while (minesPlaced < board.getmMines()) {
            int x = random.nextInt(board.getmWidth());
            int y = random.nextInt(board.getmHeight());
            char[][] minefield = board.getmMinefield();
            if (board.getmMinefield()[y][x] != '*') {
                board.getmMinefield()[y][x] = '*';
                minesPlaced++;
            }
        }
    }

    public void clearMinefield() {
        for (int y = 0; y < board.getmHeight(); y++) {
            for (int x = 0; x < board.getmWidth(); x++) {
                board.getmMinefield()[y][x] = ' ';
            }
        }
    }

    public void calculateHints() {
        for (int y = 0; y < board.getmHeight(); y++) {
            for (int x = 0; x < board.getmWidth(); x++) {
                if (board.getmMinefield()[y][x] != '*') {
                    board.getmMinefield()[y][x] = minesNear(y, x);
                }
            }
        }
    }

    private char minesNear(int y, int x) {
        int mines = 0;
        mines += mineAt(y - 1, x - 1);  // NW
        mines += mineAt(y - 1, x);      // N
        mines += mineAt(y - 1, x + 1);  // NE
        mines += mineAt(y, x - 1);      // W
        mines += mineAt(y, x + 1);      // E
        mines += mineAt(y + 1, x - 1);  // SW
        mines += mineAt(y + 1, x);      // S
        mines += mineAt(y + 1, x + 1);  // SE
        if (mines > 0) {
            return (char) (mines + 48);
        } else {
            return ' ';
        }
    }

    public int mineAt(int y, int x) {
        if (y >= 0 && y < board.getmHeight() && x >= 0 && x < board.getmWidth() && board.getmMinefield()[y][x] == '*') {
            return 1;
        } else {
            return 0;
        }
    }

    public Board getBoard() {
        return board;
    }
}
