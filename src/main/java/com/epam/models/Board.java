package com.epam.models;

public class Board {

    private int mWidth;
    private int mHeight;
    private int mMines ;
    private char[][] mMinefield;

    public Board(int mWidth, int mHeight, int mMines, char[][] mMinefield) {
        this.mWidth = mWidth;
        this.mHeight = mHeight;
        this.mMines = mMines;
        this.mMinefield = mMinefield;
    }

    public Board() {
    }

    public int getmWidth() {
        return mWidth;
    }

    public void setmWidth(int mWidth) {
        this.mWidth = mWidth;
    }

    public int getmHeight() {
        return mHeight;
    }

    public void setmHeight(int mHeight) {
        this.mHeight = mHeight;
    }

    public int getmMines() {
        return mMines;
    }

    public void setmMines(int mMines) {
        this.mMines = mMines;
    }

    public char[][] getmMinefield() {
        return mMinefield;
    }

    public char[][] setmMinefield(char[][] mMinefield) {
        this.mMinefield = mMinefield;
        return mMinefield;
    }


}
